/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kpackage;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Kaoutar
 */
public class clearitems extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet clearitems</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet clearitems at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
         HttpSession session = request.getSession();
        
        ArrayList<Item> arrayList = new ArrayList<Item >();
       
        if(request.getServletContext().getAttribute("cartItemList") != null){
            arrayList = (ArrayList<Item>)request.getServletContext().getAttribute("cartItemList");
            arrayList.clear();
        }
        
        
         //envoyer le sous total
        session.setAttribute("subtotal", 0.0);
        
        //envoyer le total des taxes
        session.setAttribute("taxes", (0.14*0.0));
        
        //envoyer le total avec taxes
        session.setAttribute("sumtotal", 0.0+(0.0*0.14));
        
        //envoyer le nombre d'items
        session.setAttribute("itemsnumber", arrayList.size());
        
        response.sendRedirect("viewcart.jsp");
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        String ID = request.getParameter("id");
        
        HttpSession session = request.getSession();
        
        ArrayList<Item> arrayList = new ArrayList<Item >();
       
        if(request.getServletContext().getAttribute("cartItemList") != null){
            arrayList = (ArrayList<Item>)request.getServletContext().getAttribute("cartItemList");
           for(Item item : arrayList){
               if(item.getID()==Integer.parseInt(ID)){
                   arrayList.remove(item);
                   break;
               }
           }
        }
        
        
        double subtotal=0.0;
        for(Item it: arrayList){
            subtotal+=it.getQtyItem()*it.getPriceItem();
        }
        
        //envoyer le sous total
        session.setAttribute("subtotal", subtotal);
        
        //envoyer le total des taxes
        session.setAttribute("taxes", (0.14*subtotal));
        
        //envoyer le total avec taxes
        session.setAttribute("sumtotal", subtotal+(subtotal*0.14));
        
        //envoyer le nombre d'items
        session.setAttribute("itemsnumber", arrayList.size());
        
        //destroy session
        session.invalidate();
       //response.getWriter().write(arrayList);
         //RequestDispatcher D = request.getRequestDispatcher("/viewcart.jsp");
        //D.forward(request, response);
        response.getWriter().write("OK"); 
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
