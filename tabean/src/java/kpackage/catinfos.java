package kpackage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import kpackage.Category;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Kaoutar
 */
public class catinfos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet catinfos</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet catinfos at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //line to print responses on the screen
        PrintWriter out = response.getWriter();
         HttpSession session = request.getSession(); //definir la session
        int catnumber=0;
        //get category
        String categories = request.getParameter("category");
        
        //instanciate category
        Category myCat = null;
        
        //instantiate product
        Product myProd;
         try {
             //call datagateway
                DatagatewayCP gcp = new DatagatewayCP();
                //get connection parametres
                Connection con = gcp.getCon();
                //prepare statement to get category
                 Statement stmt = con.createStatement();
                 //search for query -- category ID
                ResultSet rs = stmt.executeQuery("select * from category where Name='"+categories+"'");
                //display 
                while(rs.next()){
                   //out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3));
                   catnumber=Integer.parseInt(rs.getString(1));
                   myCat= new Category(catnumber, categories, rs.getString(3));
                    //out.println(catnumber); --OK
               }
                
                 //prepare statement for product
                 Statement stmtp = con.createStatement();
                 
                 //chercher la liste des produits qui conviennent a la categorie selectionnee
                ResultSet rsp = stmt.executeQuery("select * from product where IdCat='"+catnumber+"'");
                
                //Initialiser la liste des produits de la categorie selectionne
                myCat.listProd.clear();
                
                
                while(rsp.next()){
                    //récuperer les produits un par un
                    myProd= new Product(Integer.parseInt(rsp.getString(1)), rsp.getString(2), rsp.getString(3), rsp.getDouble(4), rsp.getString(5));
                    
                    //ajouter chaque produit dans la liste des produits de la categorie
                    myCat.listProd.add(myProd);
                    
               }
        
          }catch(Exception ex){
              out.println(ex.getMessage());
          }
          
        //request.setAttribute("cat", myCat);
        session.setAttribute("myCat", myCat);
        RequestDispatcher D = request.getRequestDispatcher("/selectedcategory.jsp");
        D.forward(request, response);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
