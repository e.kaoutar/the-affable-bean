/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kpackage;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Kaoutar
 */
public class prodinfos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet prodinfos</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet prodinfos at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String idd = request.getParameter("id").trim();
		
		
        String greetings = "Hello " + idd;
		
        response.setContentType("text/plain");
        response.getWriter().write(greetings);
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //line to print responses on the screen
        PrintWriter out = response.getWriter();
         HttpSession session = request.getSession(); //definir la session
         
         //id received
         int id = Integer.parseInt(request.getParameter("id"));
        
        //instantiate product
        Product myProd=new Product();
         try {
             //call datagateway
                DatagatewayCP gcp = new DatagatewayCP();
                //get connection parametres
                Connection con = gcp.getCon();
                //prepare statement to get category
                 Statement stmt = con.createStatement();
                 //search for query -- category ID
                ResultSet rsp = stmt.executeQuery("select * from product where Id="+id);
                //display 
                while(rsp.next()){
                   //out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3));
                   myProd= new Product(Integer.parseInt(rsp.getString(1)), rsp.getString(2), rsp.getString(3), rsp.getDouble(4), rsp.getString(5));
                    //out.println(catnumber); --OK
               }
                
                 
                
                //myCat.listProd.forEach((n)-> out.println(n.Id() + " " + n.Name()));
                //out.println(myCat.Name());
        
          }catch(Exception ex){
              out.println(ex.getMessage());
          }
          
        //request.setAttribute("cat", myCat);
         //session.setAttribute("prds", myProd);
        //RequestDispatcher D = request.getRequestDispatcher("/selectedcategory.jsp");
        //D.forward(request, response);
        
        response.getWriter().write(myProd.Name()+";"+myProd.Description()+";"+myProd.Price()+";"+myProd.Image());
            
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
