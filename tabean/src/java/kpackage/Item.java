/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kpackage;

/**
 *
 * @author Kaoutar
 */
public class Item {
    
    private int ID;
    private String nameItem;
    private String DescItem;
    private Double priceItem;
    private String ImgItem;
    private int qtyItem;

    /**
     * @return the nameItem
     */
    public String getNameItem() {
        return nameItem;
    }

    /**
     * @param nameItem the nameItem to set
     */
    public void setNameItem(String nameItem) {
        this.nameItem = nameItem;
    }

    /**
     * @return the DescItem
     */
    public String getDescItem() {
        return DescItem;
    }

    /**
     * @param DescItem the DescItem to set
     */
    public void setDescItem(String DescItem) {
        this.DescItem = DescItem;
    }

    /**
     * @return the priceItem
     */
    public Double getPriceItem() {
        return priceItem;
    }

    /**
     * @param priceItem the priceItem to set
     */
    public void setPriceItem(Double priceItem) {
        this.priceItem = priceItem;
    }

    /**
     * @return the ImgItem
     */
    public String getImgItem() {
        return ImgItem;
    }

    /**
     * @param ImgItem the ImgItem to set
     */
    public void setImgItem(String ImgItem) {
        this.ImgItem = ImgItem;
    }
    
    
    public Item(String nameItem, String descItem, Double priceitem, String imgItem, int qtyItem){
        this.nameItem = nameItem;
        this.DescItem = descItem;
        this.priceItem = priceitem;
        this.ImgItem = imgItem;
        this.qtyItem = qtyItem;
    }
    
    public Item(int ID, String name, String price, String qty){
        this.ID=ID;
        this.nameItem = name;
        this.priceItem=Double.parseDouble(price);
        this.qtyItem=Integer.parseInt(qty);
    }

    /**
     * @return the qtyItem
     */
    public int getQtyItem() {
        return qtyItem;
    }

    /**
     * @param qtyItem the qtyItem to set
     */
    public void setQtyItem(int qtyItem) {
        this.qtyItem = qtyItem;
    }

    /**
     * @return the ID
     */
    public int getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(int ID) {
        this.ID = ID;
    }
    
    
}
