package kpackage;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Kaoutar
 */
public class DatagatewayCP {
    
    //les variables qui permettent de se connecter a la base
    private Connection myCon;
    private PreparedStatement allCategories, allProducts, singleProduct;
    private ResultSet result;
    
    
    
    public Connection getCon() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            try {
               this.myCon = DriverManager.getConnection("jdbc:mysql://localhost:3306/tabean", "root", "");
            } catch (SQLException ex) {
               System.out.println(ex.getMessage());
            }
        } catch (ClassNotFoundException ex) {
           System.out.println(ex.getMessage());
        }
        return myCon;
    }
    
    
    
}
