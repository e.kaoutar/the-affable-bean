package kpackage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Kaoutar
 */
public class Product {
    private int id;
    private String name;
    private String description;
    private Double price;
    private String image;
    
    
    //Getters
    public int Id(){
        return this.id;
    }
    
    public String Name(){
        return this.name;
    }
    
    public String Description(){
        return this.description;
    }
    
    public Double Price(){
        return this.price;
    }
    
    public String Image(){
        return this.image;
    }
    
    //Setters
    public void setName(String name){
        this.name = name;
    }
    
    public void setDesc(String desc){
        this.description = desc;
    }
    
    public void setImage(String img){
        this.image = img;
    }
    
    public void setPrice(Double price){
        this.price = price;
    }
    
    //constructers
    public Product(){
        this.name="";
        this.description="";
        this.image="";
        this.price=0.0;
    }
    
    public Product(String name, String desc, Double price, String img){
        this.name = name;
        this.description = desc;
        this.price = price;
        this.image = img;
    }
    
        public Product(int id, String name, String desc, Double price, String img){
            
        this.id=id;
        this.name = name;
        this.description = desc;
        this.price = price;
        this.image = img;
        
    }
    

}
