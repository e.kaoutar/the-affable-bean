package kpackage;


import java.io.PrintWriter;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Kaoutar
 */


public class Category {

    private int id;
    private String name;
    private String description;
    public ArrayList<Product> listProd = new ArrayList<Product>();
    private boolean active;
    
    
    public int Id(){
        return this.id;
    }
    
    public String Name(){
        return this.name;
    }
    
    public String Description(){
        return this.description;
    }
    
    
    public boolean Active(){
        return this.active;
    }
    
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setDesc(String desc){
        this.description = desc;
    }
    
    
    public void setActive(boolean Px){
        this.active = Px;
    }
    
    

    public Category(){
        this.name="";
        this.description="";
        //initialize products array??
    }
    
    public Category(int id, String name, String desc){
        this.id=id;
        this.name = name;
        this.description = desc;
    }
    
    public Category(String name, String desc, Product listProd, boolean active){
        this.name = name;
        this.description = desc;
        this.active = active;
    }
    
    
    public Category(String name, String desc, Product listProd){
        this.name = name;
        this.description = desc;
        this.listProd=this.listProd;
    }
    
     public Category(String nomCat){
        this.listProd = new ArrayList<Product>();
        this.name=nomCat;
    }
    
     //add product function to be called just after getting data from Product table (Servlet)
    public void addProd(Product p){
        this.listProd.add(p);
    }
    
    public ArrayList<Product> getListProd(){
            return this.listProd;
    }
    
}
