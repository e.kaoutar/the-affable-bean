<%-- 
    Document   : selectedcategory
    Created on : Sep 2, 2019, 3:24:37 PM
    Author     : Kaoutar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="kpackage.Category, kpackage.Product"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>The Affable Bean - Categories</title>
        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/cssstylefooter.css" rel="stylesheet"/>
    </head>
    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
          <div class="container">
            <a class="navbar-brand" href="#">The Affable Bean</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
              <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="index.html">Home
                  </a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="viewcart.jsp">View Cart <span class="badge badge-dark" id="totalItemsCart"></span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#"> </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">English</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">|</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Cesky</a>
                </li>

              </ul>
            </div>
          </div>
        </nav>
        <br/><br/><br/>
        <span id="test"></span>
                        <% Category cx = (Category)session.getAttribute("myCat"); %>
                                        <% if(cx.Name().equals("Bakery")){%>
                                                <button type="button" class="btn btn-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Category selected" style="margin-left: 65%; width: 30%; margin-bottom: 40px;">
                                                     <%= cx.Name() %>
                                                </button>
                                        <%} else if(cx.Name().equals("Dairy")) {%>
                                                <button type="button" class="btn btn-secondary" data-container="body" data-toggle="popover" data-placement="top" data-content="Category selected" style="margin-left: 65%; width: 30%; margin-bottom: 40px;">
                                                     <%= cx.Name() %>
                                                </button>
                                        <%} else if(cx.Name().equals("Fruits and Vegetables")) {%>
                                                <button type="button" class="btn btn-danger" data-container="body" data-toggle="popover" data-placement="top" data-content="Category selected" style="margin-left: 65%; width: 30%; margin-bottom: 40px;">
                                                     <%= cx.Name() %>
                                                </button>
                                        <%} else if(cx.Name().equals("Meat")) {%>
                                                <button type="button" class="btn btn-warning" data-container="body" data-toggle="popover" data-placement="top" data-content="Category selected" style="margin-left: 65%; width: 30%; margin-bottom: 40px;">
                                                     <%= cx.Name() %>
                                                </button>
                                            <%}%>
                                            

                                 <!-- AFFICHER LES ENREGISTREMENTS : PRODUITS DE LA CATEGORIE SELECTIONNEE-->
                                 
                                 <div class="row" style="margin-bottom: 10%;">
                                             <div class="col-4">
                                                       <div class="list-group" id="list-tab" role="tablist" style="margin-left: 20px; margin-bottom: 20px;">
                                                                                <% for(Product p : cx.listProd){ %>
                                                                                <%if (p.Id()==cx.listProd.get(0).Id()){ %>
                                                                                        <a class="list-group-item list-group-item-action active listprods" id="<%= p.Id()%>" data-toggle="list" href="#<%= p.Id()%>" role="tab" aria-controls="home">Produit numéro: #<span id="idprod"><%= p.Id() %></span></a>
                                                                                <%}else{ %>
                                                                                <a class="list-group-item list-group-item-action listprods" id="<%= p.Id()%>" data-toggle="list" href="#<%= p.Id()%>" role="tab" aria-controls="home">Produit numéro: #<span id="idprod"><%= p.Id() %></span></a>
                                                                                <%}%>
                                                                                <%}%>
  
                                                        </div>
                                            </div>
                                          
                                          <div class="col-8">
                                              
   
                                                            <div class="tab-content" id="nav-tabContent" id="results">
                                                                <img id="img" src="images/<%= cx.listProd.get(0).Image()%>" class="rounded float-left" alt="..." name="imgItem">
                                                                <ul class="list-group list-group-flush">
                                                                    <li class="list-group-item">Name: <span id="nomprod" name="nameItem"><%= cx.listProd.get(0).Name()%></span></li>
                                                                        <li class="list-group-item">Description: <span id="descprod" name="descItem"><%= cx.listProd.get(0).Description()%></span></li>
                                                                        <li class="list-group-item">Price: $<span id="priceprod" name="priceItem"><%= cx.listProd.get(0).Price() %></span></li>
                                                                        <li class="list-group-item"><input type="number" name="quantityItem" value="1" min="1" max="5" class="form-control" style="width:20%" id="qtyItem"></span></li>
                                                                </ul>
                                                            </div>
                                                                        <button  class="btn btn-light addCart" style="width:30%; margin-left: 45%; "  type="submit"><img src="https://img.icons8.com/ios-filled/16/000000/add-shopping-cart.png">Add to cart</button>
                                            
                                           </div>
                                                                        

  
                                        </div>
                                

                                                <div class="mt-5 pt-5 pb-5 footer">
                                                    <div class="container">
                                                        <div class="row">
                                                          <div class="col-lg-5 col-xs-12 about-company">
                                                            <h2>Eco-friendly</h2>
                                                            <p class="pr-5 text-white-50"> We are far from perfect but I think we are slowly eradicating the myth that good-quality food is just for rich people. Most of our prices are affordable and comparable with big supermarkets, even though we have higher costs. </p>
                                                            <p><a href="#"><i class="fa fa-facebook-square mr-1"></i></a><a href="#"><i class="fa fa-linkedin-square"></i></a></p>
                                                          </div>
                                                          <div class="col-lg-3 col-xs-12 links">
                                                            <h4 class="mt-lg-0 mt-sm-3">Links</h4>
                                                              <ul class="m-0 p-0">
                                                                <li>- <a href="#">Private policies</a></li>
                                                                <li>- <a href="#">Portefolio</a></li>
                                                                <li>- <a href="#">Events</a></li>
                                                              </ul>
                                                          </div>
                                                          <div class="col-lg-4 col-xs-12 location">
                                                            <h4 class="mt-lg-0 mt-sm-4">Location</h4>
                                                            <p>3030 Rue Hochelaga, Montréal, QC H1W 1G2</p>
                                                            <p class="mb-0"><i class="fa fa-phone mr-3"></i>(541) 754-3010</p>
                                                            <p><i class="fa fa-envelope-o mr-3"></i>info@theaffablebean.ca</p>
                                                          </div>
                                                        </div>
                                                        <div class="row mt-5">
                                                          <div class="col copyright">
                                                            <p class=""><small class="text-white-50">© 2019. All Rights Reserved.</small></p>
                                                          </div>
                                                        </div>
                                                    </div>
                                                </div>


  <!-- Bootstrap core JavaScript -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script>
            //alert("javascript is on");
             //document.getElementById("test").innerHTML = "srdtfh";
        $(document).ready(function(){
            
            //$("#test").text("Dolly Duck"); -- OK
            $(".listprods").click(function(event){
               // alert("item's here!");
               event.preventDefault();
               var id=$(this).attr("id");
               //alert(id); OK
               $.post("/tabean/prodinfos", {id:id}, function(data){
                        var infoprod=data.split(";");
                        $("#nomprod").html(infoprod[0]);
                        $("#descprod").html(infoprod[1]);
                        $("#priceprod").html(infoprod[2]);
                        $("#img").attr("src", "images/"+infoprod[3]);
               });
               
            });
           
            $(".addCart").click(function(event){

                     event.preventDefault();
                     //alert("HHHHHHHHHHHHHHHHHHH"); 
                           
                    var nameItem = $("#nomprod").text();
                    var descItem = $("#descprod").text();
                    var priceItem = $("#priceprod").text();
                    var imgItem = $("#img").attr("src");
                    var qtyItem = $("#qtyItem").val();
                    
                    //alert(nameItem); 
                    
                   $.post("/tabean/addtoCart", {name:nameItem, price:priceItem, qty:qtyItem}, function(data){
                       //alert(data);
                       var infos = data.split(";");
                       alert(infos[0]);
                       document.getElementById("totalItemsCart").innerHTML=infos[1];
                   });
                    

           });
            
    });
     
    
  </script>
  
    </body>
</html>
