<%-- 
    Document   : viewcart
    Created on : Nov 23, 2019, 7:00:55 PM
    Author     : Kaoutar
--%>

<%@page import="kpackage.Item"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8" import="kpackage.Item"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/cssstylefooter.css" rel="stylesheet"/>
        <title>Your cart</title>
    </head>
    <body>
         <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
          <div class="container">
            <a class="navbar-brand" href="#">The Affable Bean</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
              <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                  <a class="nav-link" href="index.html">Home
                  </a>
                </li>

                <li class="nav-item active">
                  <a class="nav-link" href="viewcart.jsp">View Cart <span class="badge badge-dark" id="totalItemsCart"></span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#"> </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">English</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">|</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Cesky</a>
                </li>

              </ul>
            </div>
          </div>
        </nav>
        
        <% int numitems = (int)session.getAttribute("itemsnumber"); %>
                    <% if(numitems>0){ 
                            if(numitems==1){ %>
                        <center><p class="badge badge-dark nav-link"><%= numitems%> item</p></center>
                        <%} else if(numitems>1){ %>                        
                        <center><p class="badge badge-dark nav-link"><%= numitems%> items</p></center>
                        <%}%>
                    <%} else{ %>
                        <center><p class="badge badge-dark nav-link">No items selected</p></center>
                      <%}%>
                      
        
      <table class="table">

        <%
            ArrayList<Item> arrayList = new ArrayList<Item>();
            if(request.getServletContext().getAttribute("cartItemList") != null){
                arrayList = (ArrayList<Item>) request.getServletContext().getAttribute("cartItemList");
                if(!arrayList.isEmpty()){%>
                <thead>
                    <tr>
                        <th scope="col">#ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Price</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
            <%
                   
                    for(Item item: arrayList){
                        
         %>
                                            <tr>
                                                <th scope="row" id="ID"><%=item.getID()%></th>
                                                <td id="name"><%=item.getNameItem()%></td>
                                                <td id="price">$<%=item.getPriceItem()%></td>
                                                <td id="qty"><%=item.getQtyItem()%></td>
                                                <td><button type="button" onclick="getId(<%=item.getID()%>)"  class="btn btn-outline-danger btn-sm"><img src="https://img.icons8.com/ios-glyphs/16/000000/delete.png"></button></td>
                                            </tr>
           <%  }    
                        
            }
            }
        else{%>
                <center><div class="alert alert-dark" role="alert">
                             No items selected
                    </div></center>
            <%}%>

     
        </tbody>
        </table>
        <a href="clearitems" class="btn btn-warning" style="margin-left: 30em; width: 30%;"><img src="https://img.icons8.com/ios-filled/16/000000/clear-shopping-cart.png"> Clear cart</a>
        
        <br/>
        <br/>
       
        <% double subtotal = (double)session.getAttribute("subtotal"); %>
        <% double taxes = (double)session.getAttribute("taxes"); %>
        <% double total = (double)session.getAttribute("sumtotal"); %>
        <div class="alert alert-secondary" role="alert">
            <% if(total!=0.0){ %>
            SUBTOTAL: <b>$<span><%= Math.round(subtotal) %> </span></b><br/>
            TOTAL TAXES: <b>$ <span><%= Math.round(taxes) %></span></b><br/>
            <hr class="my-4">
            TOTAL: <b>$ <span><%= total %></span></b>
            <%} else{%>
            SUBTOTAL: <b>$<span>0.0</span></b><br/>
            TOTAL TAXES: <b>$ <span>0.0</span></b><br/>
            <hr class="my-4">
            TOTAL: <b>$ <span>0.0</span></b>
            <%}%>
        </div>
        
        
        
        <center><a type="button" class="btn btn-light btn-lg" href="index.html"><img src="https://img.icons8.com/pastel-glyph/32/000000/shopping-cart--v1.png"> Continue shopping &raquo; </a></center><br>
        
        <form action="https://www.sandbox.paypal.com/cgi-bin/websrc" method="post"> 
            
                                <input type="hidden" name="cmd" value="_cart"/>
                                <input type="hidden" name="upload" value="1"/>
                                <input type="hidden" name="business" value="tabeanshop@businessshop.com"/>
                                
                     <% 
                                    for(Item item: arrayList){ 
                     %>           
                              
                                <input type="hidden" name="item_name_<%=item.getID()%>" value="<%=item.getNameItem()%>"/>
                                <input type="hidden" name="item_price_<%=item.getID()%>" value="<%=item.getPriceItem()%>"/>
                                <input type="hidden" name="item_qty_<%=item.getID()%>" value="<%=item.getQtyItem()%>"/>
                                
                     <%  }  %>
                     
                                <input type="hidden" name="currency_code" value="CAD"/>
                                <input type="hidden" name="return" value="http://localhost/tabean/index.html"/>
                                <input type="hidden" name="cancel_return" value="https://www.google.com"/>
            
                                <center><button  class="btn btn-success btn-lg" type="submit" name="submit" style="width: 16%;">Checkout to paypal <img src="https://img.icons8.com/material-rounded/32/000000/shopping.png"></button></center>
        </form>
        
        
        
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
            <script>
                    function getId(itemID){
                        //alert(itemID);
                      $.post("/tabean/clearitems", {id:itemID}, function(data){
                                 if(data=="OK"){
                                     document.location.reload(true);
                                 }
                   });
                    }
                
            </script>
    </body>
</html>
